# UnityDots Common

UnityDots Common contains reusable extension methods, collections, utility methods, editor components, and more. It is a dependency for most of the UnityDots packages.
