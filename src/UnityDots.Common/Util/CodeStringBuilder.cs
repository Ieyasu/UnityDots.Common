using System;
using System.Text;
using System.Globalization;
using UnityEngine;

namespace UnityDots
{
    public class CodeStringBuilder
    {
        public string IndentationString { get; set; }
        public string LineCommentString { get; set; }
        public string ScopeBeginString { get; set; }
        public string ScopeEndString { get; set; }

        private int _indentationLevel;
        private readonly StringBuilder _strBuilder = new StringBuilder();

        public CodeStringBuilder()
            : this(0)
        {
        }

        public CodeStringBuilder(int indentationLevel)
        {
            IncreaseIndent(indentationLevel);
            IndentationString = "    ";
            LineCommentString = "// ";
            ScopeBeginString = "{";
            ScopeEndString = "}";
        }

        public void Clear()
        {
            _strBuilder.Clear();
        }

        public void Append(string value)
        {
            _strBuilder.Append(value);
        }

        public void Append(CodeStringBuilder other)
        {
            _strBuilder.Append(other);
        }

        public void AppendFormat(string formatString, params object[] args)
        {
            _strBuilder.AppendFormat(CultureInfo.InvariantCulture, formatString, args);
        }

        public void AppendLine()
        {
            _strBuilder.AppendLine();
        }

        public void AppendLine(string value)
        {
            AppendIndentation();
            _strBuilder.AppendLine(value);
        }

        public void AppendFormatLine(string formatString, params object[] args)
        {
            AppendIndentation();
            _strBuilder.AppendFormat(CultureInfo.InvariantCulture, formatString, args);
            _strBuilder.AppendLine();
        }

        public void AppendLineComment(string value)
        {
            AppendIndentation();
            Append(LineCommentString);
            AppendLine(value);
        }

        public void AppendFormatLineComment(string formatString, params object[] args)
        {
            AppendIndentation();
            Append(LineCommentString);
            AppendFormatLine(formatString, args);
        }

        public void AppendLines(string lines)
        {
            var splitLines = lines.Split('\n');
            var lineCount = splitLines.Length;
            var lastLine = splitLines[lineCount - 1];

            if (string.IsNullOrEmpty(lastLine) || lastLine == "\r")
            {
                lineCount--;
            }

            for (var i = 0; i < lineCount; ++i)
            {
                AppendLine(splitLines[i].Trim('\r'));
            }
        }

        public void AppendIndentation()
        {
            for (var i = 0; i < _indentationLevel; ++i)
            {
                _strBuilder.Append(IndentationString);
            }
        }

        public void AppendBlock()
        {
            AppendLine(ScopeBeginString);
            AppendLine(ScopeEndString);
            AppendLine();
        }

        public void AppendBlock(CodeStringBuilder other)
        {
            AppendLine(ScopeBeginString);
            IncreaseIndent();
            AppendLines(other.ToString());
            DecreaseIndent();
            AppendLine(ScopeEndString);
            AppendLine();
        }

        public void AppendBlock(Action action)
        {
            AppendLine(ScopeBeginString);
            IncreaseIndent();
            action.Invoke();
            DecreaseIndent();
            AppendLine(ScopeEndString);
            AppendLine();
        }

        public void IncreaseIndent(int level = 1)
        {
            _indentationLevel += Mathf.Max(0, level);
        }

        public void DecreaseIndent(int level = 1)
        {
            _indentationLevel = Mathf.Max(0, _indentationLevel - Mathf.Max(0, level));
        }

        public override string ToString()
        {
            return _strBuilder.ToString();
        }
    }
}
