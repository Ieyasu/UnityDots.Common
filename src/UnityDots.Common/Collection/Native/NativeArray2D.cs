using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Collections
{
    public struct NativeArray2D<T> : IEquatable<NativeArray2D<T>>, IDisposable where T : struct
    {
        private NativeArray<T> _array;

        public bool IsCreated => _array.IsCreated;
        public int Length => _array.Length;
        public int Width { get; private set; }
        public int Height { get; private set; }

        public T this[int index]
        {
            get => _array[index];
            set => _array[index] = value;
        }

        public T this[int y, int x]
        {
            get => _array[y * Width + x];
            set => _array[y * Width + x] = value;
        }

        public NativeArray2D(NativeArray<T> array, int width, int height)
        {
            if (array.Length != width * height)
            {
                throw new ArgumentException($"Array (length {array.Length}) must match the width ({width}) and height ({height})");
            }

            _array = array;
            Width = width;
            Height = height;
        }

        public NativeArray2D(int width, int height, Allocator allocator, NativeArrayOptions options = NativeArrayOptions.ClearMemory)
        {
            _array = new NativeArray<T>(width * height, allocator, options);
            Width = width;
            Height = height;
        }

        public void Dispose()
        {
            _array.Dispose();
        }

        public T GetValue(int x, int y)
        {
            return this[y, x];
        }

        public void CopyFrom(NativeArray<T> array)
        {
            _array.CopyFrom(array);
        }

        public void CopyTo(NativeArray<T> array)
        {
            _array.CopyTo(array);
        }

        public override bool Equals(object obj)
        {
            return obj is NativeArray2D<T> other && Equals(other);
        }

        public bool Equals(NativeArray2D<T> other)
        {
            return Width == other.Width
                    && Height == other.Height
                    && EqualityComparer<NativeArray<T>>.Default.Equals(_array, other._array);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_array, Width, Height);
        }
    }

    public static class NativeArray2DExtensions
    {
        public static float GetInterpolatedValue(this NativeArray2D<int> array, float x, float y)
        {

            var xC = x * array.Width;
            var yC = y * array.Height;
            var xLo = (int)clamp(xC, 0, array.Width - 1);
            var yLo = (int)clamp(yC, 0, array.Height - 1);
            var xHi = xLo == array.Width - 1 ? xLo : xLo + 1;
            var yHi = yLo == array.Height - 1 ? yLo : yLo + 1;
            var xFrac = xC - xLo;
            var yFrac = yC - yLo;

            var val11 = array[yLo, xLo];
            var val12 = array[yLo, xHi];
            var val1 = val11 + xFrac * (val12 - val11);
            var val21 = array[yHi, xLo];
            var val22 = array[yHi, xHi];
            var val2 = val21 + xFrac * (val22 - val21);
            return val1 + yFrac * (val2 - val1);
        }

        public static float GetInterpolatedValue(this NativeArray2D<float> array, float x, float y)
        {
            var xC = x * array.Width;
            var yC = y * array.Height;
            var xLo = (int)clamp(xC, 0, array.Width - 1);
            var yLo = (int)clamp(yC, 0, array.Height - 1);
            var xHi = xLo == array.Width - 1 ? xLo : xLo + 1;
            var yHi = yLo == array.Height - 1 ? yLo : yLo + 1;
            var xFrac = xC - xLo;
            var yFrac = yC - yLo;

            var val11 = array[yLo, xLo];
            var val12 = array[yLo, xHi];
            var val1 = val11 + xFrac * (val12 - val11);
            var val21 = array[yHi, xLo];
            var val22 = array[yHi, xHi];
            var val2 = val21 + xFrac * (val22 - val21);
            return val1 + yFrac * (val2 - val1);
        }

        public static float3 GetInterpolatedValue(this NativeArray2D<float3> array, float x, float y)
        {

            var xC = x * array.Width;
            var yC = y * array.Height;
            var xLo = (int)clamp(xC, 0, array.Width - 1);
            var yLo = (int)clamp(yC, 0, array.Height - 1);
            var xHi = xLo == array.Width - 1 ? xLo : xLo + 1;
            var yHi = yLo == array.Height - 1 ? yLo : yLo + 1;
            var xFrac = xC - xLo;
            var yFrac = yC - yLo;

            var val11 = array[yLo, xLo];
            var val12 = array[yLo, xHi];
            var val1 = val11 + xFrac * (val12 - val11);
            var val21 = array[yHi, xLo];
            var val22 = array[yHi, xHi];
            var val2 = val21 + xFrac * (val22 - val21);
            return val1 + yFrac * (val2 - val1);
        }
    }
}
