using UnityEngine;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using System.Collections.Generic;

namespace UnityDots.Collections
{
    public static class NativeArrayUtil
    {
        public static void CopyFrom(this NativeArray<int> destination, List<int> source)
        {
            for (var i = 0; i < destination.Length; ++i)
            {
                destination[i] = source[i];
            }
        }

        public static void CopyFrom(this NativeArray<float2> destination, List<Vector2> source)
        {
            for (var i = 0; i < destination.Length; ++i)
            {
                destination[i] = source[i];
            }
        }

        public static void CopyFrom(this NativeArray<float3> destination, List<Vector3> source)
        {
            for (var i = 0; i < destination.Length; ++i)
            {
                destination[i] = source[i];
            }
        }

        public static void CopyFrom(this NativeArray<float4> destination, List<Vector4> source)
        {
            for (var i = 0; i < destination.Length; ++i)
            {
                destination[i] = source[i];
            }
        }

        public static void CopyFrom(this NativeArray<float4> destination, List<Color> source)
        {
            for (var i = 0; i < destination.Length; ++i)
            {
                destination[i] = (Vector4)source[i];
            }
        }

        public static void CopyTo(this NativeArray<int> source, List<int> destination)
        {
            for (var i = 0; i < source.Length; ++i)
            {
                destination[i] = source[i];
            }
        }

        public static void CopyTo(this NativeArray<float2> source, List<Vector2> destination)
        {
            for (var i = 0; i < source.Length; ++i)
            {
                destination[i] = source[i];
            }
        }

        public static void CopyTo(this NativeArray<float3> source, List<Vector3> destination)
        {
            for (var i = 0; i < source.Length; ++i)
            {
                destination[i] = source[i];
            }
        }

        public static void CopyTo(this NativeArray<float4> source, List<Vector4> destination)
        {
            for (var i = 0; i < source.Length; ++i)
            {
                destination[i] = source[i];
            }
        }

        public static void CopyTo(this NativeArray<float4> source, List<Color> destination)
        {
            for (var i = 0; i < source.Length; ++i)
            {
                destination[i] = (Vector4)source[i];
            }
        }

        public static unsafe void CopyFromUnsafe(this NativeArray<int> destination, int[] source)
        {
            fixed (void* managedArrayPointer = source)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(destination),
                    source: managedArrayPointer,
                    size: source.Length * (long)UnsafeUtility.SizeOf<int>()
                );
            }
        }

        public static unsafe void CopyFromUnsafe(this NativeArray<float> destination, float[] source)
        {
            fixed (void* managedArrayPointer = source)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(destination),
                    source: managedArrayPointer,
                    size: source.Length * (long)UnsafeUtility.SizeOf<float>()
                );
            }
        }

        public static unsafe void CopyFromUnsafe(this NativeArray<float2> destination, Vector2[] source)
        {
            fixed (void* managedArrayPointer = source)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(destination),
                    source: managedArrayPointer,
                    size: source.Length * (long)UnsafeUtility.SizeOf<Vector2>()
                );
            }
        }

        public static unsafe void CopyFromUnsafe(this NativeArray<float3> destination, Vector3[] source)
        {
            fixed (void* managedArrayPointer = source)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(destination),
                    source: managedArrayPointer,
                    size: source.Length * (long)UnsafeUtility.SizeOf<Vector3>()
                );
            }
        }

        public static unsafe void CopyFromUnsafe(this NativeArray<float4> destination, Vector4[] source)
        {
            fixed (void* managedArrayPointer = source)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(destination),
                    source: managedArrayPointer,
                    size: source.Length * (long)UnsafeUtility.SizeOf<Vector4>()
                );
            }
        }

        public static unsafe void CopyFromUnsafe(this NativeArray<float4> destination, Color[] source)
        {
            fixed (void* managedArrayPointer = source)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(destination),
                    source: managedArrayPointer,
                    size: source.Length * (long)UnsafeUtility.SizeOf<Color>()
                );
            }
        }

        public static unsafe void CopyToUnsafe(this NativeArray<int> source, int[] destination)
        {
            fixed (void* managedArrayPointer = destination)
            {
                UnsafeUtility.MemCpy
                (
                    destination: managedArrayPointer,
                    source: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(source),
                    size: destination.Length * (long)UnsafeUtility.SizeOf<int>()
                );
            }
        }

        public static unsafe void CopyToUnsafe(this NativeArray<float> source, float[] destination)
        {
            fixed (void* managedArrayPointer = destination)
            {
                UnsafeUtility.MemCpy
                (
                    destination: managedArrayPointer,
                    source: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(source),
                    size: destination.Length * (long)UnsafeUtility.SizeOf<int>()
                );
            }
        }

        public static unsafe void CopyToUnsafe(this NativeArray<float2> source, Vector2[] destination)
        {
            fixed (void* managedArrayPointer = destination)
            {
                UnsafeUtility.MemCpy
                (
                    destination: managedArrayPointer,
                    source: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(source),
                    size: destination.Length * (long)UnsafeUtility.SizeOf<Vector2>()
                );
            }
        }

        public static unsafe void CopyToUnsafe(this NativeArray<float3> source, Vector3[] destination)
        {
            fixed (void* managedArrayPointer = destination)
            {
                UnsafeUtility.MemCpy
                (
                    destination: managedArrayPointer,
                    source: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(source),
                    size: destination.Length * (long)UnsafeUtility.SizeOf<Vector3>()
                );
            }
        }

        public static unsafe void CopyToUnsafe(this NativeArray<float4> source, Vector4[] destination)
        {
            fixed (void* managedArrayPointer = destination)
            {
                UnsafeUtility.MemCpy
                (
                    destination: managedArrayPointer,
                    source: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(source),
                    size: destination.Length * (long)UnsafeUtility.SizeOf<Vector4>()
                );
            }
        }

        public static unsafe void CopyToUnsafe(this NativeArray<float4> source, Color[] destination)
        {
            fixed (void* managedArrayPointer = destination)
            {
                UnsafeUtility.MemCpy
                (
                    destination: managedArrayPointer,
                    source: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(source),
                    size: destination.Length * (long)UnsafeUtility.SizeOf<Color>()
                );
            }
        }
    }
}
