using System.Collections.Generic;

namespace UnityDots
{
    public static class ArrayExtensions
    {
        public static bool SequenceEquals<T>(this T[] first, T[] second)
        {
            return first.SequenceEquals(second, EqualityComparer<T>.Default);
        }

        public static bool SequenceEquals<T>(this T[] first, T[] second, IEqualityComparer<T> comparer)
        {
            if (ReferenceEquals(first, second))
            {
                return true;
            }

            if (second == null)
            {
                return false;
            }

            if (first.Length != second.Length)
            {
                return false;
            }

            for (var i = 0; i < first.Length; ++i)
            {
                if (!comparer.Equals(first[i], second[i]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
