using System.Collections.Generic;

namespace UnityDots
{
    public static class ListExtensions
    {
        public static void Resize<T>(this List<T> list, int size)
        {
            Resize(list, size, default);
        }

        public static void Resize<T>(this List<T> list, int size, T item)
        {
            var current = list.Count;
            if (size < current)
            {
                list.RemoveRange(size, current - size);
            }
            else if (size > current)
            {
                if (size > list.Capacity)
                {
                    list.Capacity = size;
                }
                for (var i = 0; i < size - current; ++i)
                {
                    list.Add(item);
                }
            }
        }

        public static bool SequenceEquals<T>(this List<T> first, List<T> second)
        {
            return first.SequenceEquals(second, EqualityComparer<T>.Default);
        }

        public static bool SequenceEquals<T>(this List<T> first, List<T> second, IEqualityComparer<T> comparer)
        {
            if (ReferenceEquals(first, second))
            {
                return true;
            }

            if (second == null)
            {
                return false;
            }

            if (first.Count != second.Count)
            {
                return false;
            }

            for (var i = 0; i < first.Count; ++i)
            {
                if (!comparer.Equals(first[i], second[i]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
