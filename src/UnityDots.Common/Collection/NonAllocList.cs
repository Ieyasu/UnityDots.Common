using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace UnityDots.Collections
{
    public sealed class NonAllocList<T> : IDisposable, IList, ICollection<T>, IEnumerable<T>
    {
        private static readonly ConcurrentBag<NonAllocList<T>> _buffers = new ConcurrentBag<NonAllocList<T>>();

        private bool _inUse;

        public int Count => Data.Count;
        public bool IsFixedSize => false;
        public bool IsReadOnly => false;
        public bool IsSynchronized => false;
        public object SyncRoot => this;

        public List<T> Data
        {
            get;
            private set;
        }

        public int Capacity
        {
            get => Data.Capacity;
            set => Data.Capacity = value;
        }

        public T this[int index]
        {
            get => Data[index];
            set => Data[index] = value;
        }

        object IList.this[int index]
        {
            get => this[index];
            set => this[index] = (T)value;
        }

        public static NonAllocList<T> Get()
        {
            if (!_buffers.TryTake(out NonAllocList<T> buffer))
            {
                buffer = new NonAllocList<T>
                {
                    Data = new List<T>()
                };
            }
            buffer._inUse = true;
            return buffer;
        }

        public void Dispose()
        {
            if (_inUse)
            {
                Data.Clear();
                _inUse = false;
                _buffers.Add(this);
            }
        }

        public void Add(T item)
        {
            Data.Add(item);
        }

        int IList.Add(object item)
        {
            return (Data as IList).Add(item);
        }

        public void Clear()
        {
            Data.Clear();
        }

        public bool Contains(T item)
        {
            return Data.Contains(item);
        }

        bool IList.Contains(object item)
        {
            return (Data as IList).Contains(item);
        }

        public int IndexOf(T item)
        {
            return Data.IndexOf(item);
        }

        int IList.IndexOf(Object item)
        {
            return (Data as IList).IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            Data.Insert(index, item);
        }

        void IList.Insert(int index, Object item)
        {
            (Data as IList).Insert(index, item);
        }

        public bool Remove(T item)
        {
            return Data.Remove(item);
        }

        void IList.Remove(Object item)
        {
            (Data as IList).Remove(item);
        }

        public void RemoveAt(int index)
        {
            Data.RemoveAt(index);
        }

        public void CopyTo(T[] array)
        {
            Data.CopyTo(array);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Data.CopyTo(array, arrayIndex);
        }

        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            (Data as ICollection).CopyTo(array, arrayIndex);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Data.GetEnumerator();
        }
    }
}
