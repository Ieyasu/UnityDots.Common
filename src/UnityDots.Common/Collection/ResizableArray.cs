using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityDots.Collections
{
    public sealed class ResizableArray<T> : IList, IEnumerable<T>
    {
        private T[] _items;

        public T[] Items => _items;
        public int Count { get; private set; }
        public bool IsFixedSize => false;
        public bool IsReadOnly => false;
        public bool IsSynchronized => false;
        public object SyncRoot => this;

        public int Capacity
        {
            get => _items.Length;
            set
            {
                if (value == _items.Length)
                {
                    return;
                }

                value = value < 0 ? 0 : value;
                Array.Resize(ref _items, value);
            }
        }

        public T this[int index]
        {
            get => _items[index];
            set => _items[index] = value;
        }

        object IList.this[int index]
        {
            get => this[index];
            set => this[index] = (T)value;
        }

        public ResizableArray()
            : this(4)
        {
        }

        public ResizableArray(int capacity)
        {
            _items = new T[capacity];
        }

        public void Add(T item)
        {
            if (Count == _items.Length)
            {
                EnsureCapacity(Count + 1);
            }

            _items[Count++] = item;
        }

        int IList.Add(object item)
        {
            Add((T)item);
            return Count - 1;
        }

        public void AddRange(IEnumerable<T> collection)
        {
            if (collection is IList<T> list)
            {
                EnsureCapacity(Count + list.Count);
            }

            foreach (var item in collection)
            {
                Add(item);
            }
        }

        public void Clear()
        {
            if (Count > 0)
            {
                Array.Clear(_items, 0, Count);
                Count = 0;
            }
        }

        public void Resize(int size)
        {
            EnsureCapacity(size);

            if (size < Count)
            {
                Array.Clear(_items, size, Count - size);
            }
            else
            {
                Array.Clear(_items, Count, size - Count);
            }

            Count = size;
        }

        public bool Contains(T item)
        {
            if (item == null)
            {
                for (var i = 0; i < Count; i++)
                {
                    if (_items[i] == null)
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                var c = EqualityComparer<T>.Default;
                for (int i = 0; i < Count; i++)
                {
                    if (c.Equals(_items[i], item))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        bool IList.Contains(object item)
        {
            return IsCompatibleObject(item) ? Contains((T)item) : false;
        }

        public int IndexOf(T item)
        {
            return Array.IndexOf(_items, item, 0, Count);
        }

        int IList.IndexOf(Object item)
        {
            return IsCompatibleObject(item) ? IndexOf((T)item) : -1;
        }

        public void Insert(int index, T item)
        {
            if (Count == _items.Length)
            {
                EnsureCapacity(Count + 1);
            }

            if (index < Count)
            {
                Array.Copy(_items, index, _items, index + 1, Count - index);
            }

            _items[index] = item;
            Count++;
        }

        void IList.Insert(int index, Object item)
        {
            Insert(index, (T)item);
        }

        public bool Remove(T item)
        {
            int index = IndexOf(item);
            if (index >= 0)
            {
                RemoveAt(index);
                return true;
            }
            return false;
        }

        void IList.Remove(Object item)
        {
            if (IsCompatibleObject(item))
            {
                Remove((T)item);
            }
        }

        public void RemoveAt(int index)
        {
            Count--;
            if (index < Count)
            {
                Array.Copy(_items, index + 1, _items, index, Count - index);
            }
            _items[Count] = default;
        }

        public void CopyTo(T[] array)
        {
            CopyTo(array, 0);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Array.Copy(_items, 0, array, arrayIndex, Count);
        }

        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            Array.Copy(_items, 0, array, arrayIndex, Count);
        }

        private void EnsureCapacity(int min)
        {
            if (_items.Length < min)
            {
                int newCapacity = _items.Length == 0 ? 4 : _items.Length * 2;

                if (newCapacity < min)
                {
                    newCapacity = min;
                }

                Capacity = newCapacity;
            }
        }

        private static bool IsCompatibleObject(object value)
        {
            return (value is T) || (value == null && default(T) == null);
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < Count; ++i)
            {
                yield return _items[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
