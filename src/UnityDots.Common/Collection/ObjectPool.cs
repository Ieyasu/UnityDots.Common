using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Collections
{
    public sealed class ObjectPool
    {
        private readonly Queue<GameObject> _pool;
        private readonly HashSet<GameObject> _set;

        public int Count => _pool.Count;
        public bool IsEmpty => _pool.Count == 0;
        public int MaxSize { get; private set; }

        public ObjectPool(int maxSize = 200)
        {
            _pool = new Queue<GameObject>();
            _set = new HashSet<GameObject>();
            MaxSize = maxSize;
        }

        // Destroys all GameObjects in the pool.
        public void Clear()
        {
            foreach (var obj in _pool)
            {
                if (Application.isPlaying)
                {
                    Object.Destroy(obj);
                }
                else
                {
                    Object.DestroyImmediate(obj);
                }
            }

            _pool.Clear();
            _set.Clear();
        }

        // Gets an existing instance from the pool.
        public GameObject Get(bool active = true)
        {
            return Get(Vector3.zero, Quaternion.identity, null, active);
        }

        // Gets an existing instance from the pool.
        public GameObject Get(Vector3 position, Quaternion rotation, Transform parent, bool active = true)
        {
            var obj = Dequeue();
            obj.transform.SetParent(parent);
            obj.transform.position = position;
            obj.transform.rotation = rotation;
            obj.SetActive(active);
            _set.Remove(obj);
            return obj;
        }

        // Adds the given GameObject to the pool.
        public bool Pool(GameObject obj)
        {
            // Check if pool if full.
            if (_pool.Count >= MaxSize)
            {
                Debug.LogWarning("Failed to pool " + obj + ": Pool full.");
                return false;
            }

            if (_set.Contains(obj))
            {
                Debug.LogWarning("Failed to pool " + obj + ": Already pooled.");
                return false;
            }

            obj.SetActive(false);
            Enqueue(obj);
            return true;
        }

        // Resizes the pools maximum capacity to the given size.
        public void Resize(int size)
        {
            MaxSize = Mathf.Max(0, size);

            // Remove overflowing instances.
            while (_pool.Count > MaxSize)
            {
                var obj = Dequeue();
                Object.Destroy(obj);
            }
        }

        private void Enqueue(GameObject obj)
        {
            _pool.Enqueue(obj);
            _set.Add(obj);
        }

        private GameObject Dequeue()
        {
            var obj = _pool.Dequeue();
            _set.Remove(obj);
            return obj;
        }
    }
}
