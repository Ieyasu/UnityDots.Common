using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityDots.Collections
{
    [Serializable]
    public class SerializableDictionary<TKey, TValue>
    {
        private Dictionary<TKey, int> _indexMap;
        private Dictionary<TKey, int> IndexMap => GetIndexMap();

        public int Count => Keys.Count;

        [SerializeField]
        private List<TKey> _keys = new List<TKey>();
        public List<TKey> Keys => _keys;

        [SerializeField]
        private List<TValue> _values = new List<TValue>();
        public List<TValue> Values => _values;

        public TValue this[TKey key]
        {
            get => Values[IndexMap[key]];
        }

        public void Add(TKey key, TValue value)
        {
            Keys.Add(key);
            Values.Add(value);
            IndexMap.Add(key, Count - 1);
        }

        public void Remove(TKey key)
        {
            var index = IndexMap[key];
            Keys.RemoveAt(index);
            Values.RemoveAt(index);
            IndexMap.Remove(key);
        }

        public bool ContainsKey(TKey key)
        {
            return IndexMap.ContainsKey(key);
        }

        public TKey GetKeyAtIndex(int index)
        {
            return _keys[index];
        }

        public TValue GetValueAtIndex(int index)
        {
            return _values[index];
        }

        private Dictionary<TKey, int> GetIndexMap()
        {
            if (_indexMap == null)
            {
                _indexMap = new Dictionary<TKey, int>();
                for (var i = 0; i < Count; ++i)
                {
                    _indexMap.Add(_keys[i], i);
                }
            }

            return _indexMap;
        }
    }
}
