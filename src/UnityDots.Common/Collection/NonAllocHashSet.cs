using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace UnityDots.Collections
{
    public sealed class NonAllocHashSet<T> : IDisposable, ISet<T>, ICollection<T>, IEnumerable
    {
        private static readonly ConcurrentBag<NonAllocHashSet<T>> _buffers = new ConcurrentBag<NonAllocHashSet<T>>();

        private bool _inUse;

        public int Count => Data.Count;
        public bool IsReadOnly => false;

        public HashSet<T> Data
        {
            get;
            private set;
        }

        public static NonAllocHashSet<T> Get()
        {
            if (!_buffers.TryTake(out NonAllocHashSet<T> buffer))
            {
                buffer = new NonAllocHashSet<T>
                {
                    Data = new HashSet<T>()
                };
            }
            buffer._inUse = true;
            return buffer;
        }

        public void Dispose()
        {
            if (_inUse)
            {
                Data.Clear();
                _inUse = false;
                _buffers.Add(this);
            }
        }

        public bool Add(T item)
        {
            return Data.Add(item);
        }

        public void Clear()
        {
            Data.Clear();
        }

        public bool Contains(T item)
        {
            return Data.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Data.CopyTo(array, arrayIndex);
        }

        public void ExceptWith(IEnumerable<T> other)
        {
            Data.ExceptWith(other);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Data.GetEnumerator();
        }

        public void IntersectWith(IEnumerable<T> other)
        {
            Data.IntersectWith(other);
        }

        public bool IsProperSubsetOf(IEnumerable<T> other)
        {
            return Data.IsProperSubsetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<T> other)
        {
            return Data.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<T> other)
        {
            return Data.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<T> other)
        {
            return Data.IsSupersetOf(other);
        }

        public bool Overlaps(IEnumerable<T> other)
        {
            return Data.Overlaps(other);
        }

        public bool Remove(T item)
        {
            return Data.Remove(item);
        }

        public bool SetEquals(IEnumerable<T> other)
        {
            return Data.SetEquals(other);
        }

        public void SymmetricExceptWith(IEnumerable<T> other)
        {
            Data.SymmetricExceptWith(other);
        }

        public void UnionWith(IEnumerable<T> other)
        {
            Data.UnionWith(other);
        }

        void ICollection<T>.Add(T item)
        {
            (Data as ICollection<T>).Add(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Data.GetEnumerator();
        }
    }
}
