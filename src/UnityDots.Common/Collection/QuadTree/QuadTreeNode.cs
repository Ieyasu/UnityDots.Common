using System;

namespace UnityDots.Collections
{
    public struct QuadTreeNode : IEquatable<QuadTreeNode>
    {
        internal int Index { get; set; }
        internal int ChildIndex { get; set; }
        internal int ParentIndex { get; set; }

        public override bool Equals(object obj)
        {
            return obj is QuadTreeNode other && Equals(other);
        }

        public bool Equals(QuadTreeNode other)
        {
            return Index == other.Index
                   && ChildIndex == other.ChildIndex
                   && ParentIndex == other.ParentIndex;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Index, ChildIndex, ParentIndex);
        }
    }
}
