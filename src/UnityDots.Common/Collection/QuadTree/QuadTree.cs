using System;
using System.Collections.Generic;
using static Unity.Mathematics.math;

namespace UnityDots.Collections
{
    public class QuadTree<TNodeData>
    {
        private readonly static Stack<QuadTreeNode> _stack = new Stack<QuadTreeNode>();

        private int _leafStartIndex;
        private int[] _indices;
        private QuadTreeNode[] _nodes;
        private TNodeData[] _nodeData;

        public int Depth { get; private set; }
        public int GridSize { get; private set; }
        public QuadTreeNode RootNode => _nodes[0];

        public QuadTree()
        {
            Resize(1);
        }

        public bool IsLeaf(QuadTreeNode node)
        {
            return node.ChildIndex < 0;
        }

        public QuadTreeNode GetLeafNode(int x, int y)
        {
            var index = _leafStartIndex + y * GridSize + x;
            return _nodes[index];
        }

        public QuadTreeNode GetChildNode(QuadTreeNode node, int child)
        {
            return _nodes[node.ChildIndex + child];
        }

        public QuadTreeNode GetParentNode(QuadTreeNode node)
        {
            return _nodes[node.ParentIndex];
        }

        public TNodeData GetData(QuadTreeNode node)
        {
            return _nodeData[node.Index];
        }

        public TNodeData GetChildData(QuadTreeNode node, int child)
        {
            return GetData(GetChildNode(node, child));
        }

        public TNodeData GetParentData(QuadTreeNode node)
        {
            return GetData(GetParentNode(node));
        }

        public void SetNodeData(QuadTreeNode node, TNodeData data)
        {
            _nodeData[node.Index] = data;
        }

        public void SetLeafData(int x, int y, TNodeData data)
        {
            var node = GetLeafNode(x, y);
            _nodeData[node.Index] = data;
        }

        public void Resize(int gridSize)
        {
            if (GridSize == gridSize)
            {
                return;
            }

            // Calculate the maximum depth of the quadtree
            GridSize = gridSize;
            Depth = (int)round(log2(GridSize)) + 1;

            if (_indices == null)
            {
                _indices = new int[Depth + 1];
            }
            else
            {
                Array.Resize(ref _indices, Depth + 1);
            }

            for (var i = 0; i < Depth + 1; ++i)
            {
                _indices[i] = ((int)round(pow(4, i)) - 1) / 3;
            }

            _leafStartIndex = GetStartIndex(Depth - 1);

            // Nodes are stored in an array for improved performance
            var nodeCount = GetNodeCount(Depth);
            if (_nodes == null)
            {
                _nodes = new QuadTreeNode[nodeCount];
                _nodeData = new TNodeData[nodeCount];
            }
            else
            {
                Array.Resize(ref _nodes, nodeCount);
                Array.Resize(ref _nodeData, nodeCount);
            }

            for (var d = 0; d < Depth; ++d)
            {
                var startIndex = GetStartIndex(d);
                var endIndex = GetEndIndex(d);
                for (var i = startIndex; i < endIndex; ++i)
                {
                    _nodes[i] = new QuadTreeNode
                    {
                        Index = i,
                        ChildIndex = GetChildIndex(i, d),
                        ParentIndex = GetParentIndex(i, d)
                    };
                }
            }
        }

        public void TraverseDFS(Func<QuadTreeNode, bool> callback)
        {
            _stack.Clear();

            TraverseDFS(0);
            while (_stack.Count > 0)
            {
                var node = _stack.Pop();
                if (callback(node) && !IsLeaf(node))
                {
                    TraverseDFS(node.ChildIndex + 3);
                    TraverseDFS(node.ChildIndex + 2);
                    TraverseDFS(node.ChildIndex + 1);
                    TraverseDFS(node.ChildIndex + 0);
                }
            }
        }

        public void TraverseBFS(Action<QuadTreeNode> callback)
        {
            foreach (var node in _nodes)
            {
                callback(node);
            }
        }

        public void TraverseReverseBFS(Action<QuadTreeNode> callback)
        {
            for (var i = _nodes.Length - 1; i >= 0; --i)
            {
                callback(_nodes[i]);
            }
        }

        private void TraverseDFS(int index)
        {
            var node = _nodes[index];
            _stack.Push(node);
        }

        private int GetChildIndex(int index, int depth)
        {
            if (depth >= Depth - 1)
            {
                return -1;
            }

            var nodeOffset = index - GetStartIndex(depth);
            var childOffset = 4 * nodeOffset;
            var childIndex = GetStartIndex(depth + 1) + childOffset;
            return childIndex;
        }

        private int GetParentIndex(int index, int depth)
        {
            if (depth == 0)
            {
                return -1;
            }

            var nodeOffset = index - GetStartIndex(depth);
            var parentOffset = (nodeOffset - nodeOffset % 4) / 4;
            var parentIndex = GetStartIndex(depth - 1) + parentOffset;
            return parentIndex;
        }

        private int GetStartIndex(int depth)
        {
            return _indices[depth];
        }

        private int GetEndIndex(int depth)
        {
            return _indices[depth + 1];
        }

        private int GetNodeCount(int depth)
        {
            return GetStartIndex(depth);
        }
    }
}
