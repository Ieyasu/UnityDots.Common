// namespace UnityDots.Collections
// {
//     public sealed class SmartList<T>
//     {
//         private struct HandleMapper
//         {
//             public int DataIndex { get; set; }
//             public int NextInactiveIndex { get; set; }
//             public int Counter { get; set; }
//             public bool IsActive { get; set; }
//         };

//         private List<T> _data;
//         private List<int> _indices;
//         private List<HandleMapper> _handleMappers;
//         private int _nextInactiveIndex;

//         public void Clear()
//         {
//             _data.clear();
//             _indices.clear();
//             _handleMappers.clear();
//             m_nextInactiveIndex = 0;
//         }

//         public void Insert(T value)
//         {
//             // Make sure there's enough room for another handle.
//             while (_handleMappers.Count <= _nextInactiveIndex)
//             {
//                 var nextFreeMapper = _handleMappers.Count + 1;
//                 handleMappers.emplace_back(nextFreeMapper);
//             }

//             // Get the next free handle entry and the corresponding index.
//             const auto newIndex = m_nextInactiveIndex;
//             auto & handleMapper = m_handleMappers[newIndex];
//             assert(!handleMapper.isActive);

//             // Update mapper connections.
//             m_nextInactiveIndex = handleMapper.nextInactiveIndex;
//             handleMapper.isActive = true;

//             handleMapper.counter++;
//             if (handleMapper.counter == CG_HANDLE_INVALID_COUNTER)
//             {
//                 handleMapper.counter = CG_HANDLE_INVALID_COUNTER + 1;
//             }
//             handleMapper.dataIndex = m_data.size();

//             m_data.push_back(value);
//             m_handleIndices.push_back(newIndex);

//             return handle_type(*this, getHandleValue(handleMapper.counter, newIndex));
//         }


//         template<class T>
//     bool HandleArray<T>::remove(const handle_value_type& handleValue)
// {
//     const auto index = getHandleIndex(handleValue);
//         const auto counter = getHandleCounter(handleValue);
//         assert(index >= 0 && index<m_handleMappers.size());

//         auto & handleMapper = m_handleMappers[index];
//     if (counter != handleMapper.counter || !handleMapper.isActive)
//     {
//         return false;
//     }

//     handleMapper.nextInactiveIndex = m_nextInactiveIndex;
//     m_nextInactiveIndex = index;
//     handleMapper.isActive = false;

//     if (size() > 1)
//     {
//         const auto&lastHandleIndex = m_handleIndices.back();
//         auto & lastHandleMapper = m_handleMappers[lastHandleIndex];
//         std::swap(m_data[handleMapper.dataIndex], m_data.back());
//         std::swap(m_handleIndices[handleMapper.dataIndex], m_handleIndices.back());
//         lastHandleMapper.dataIndex = handleMapper.dataIndex;
//     }

//     m_data.pop_back();
//     m_handleIndices.pop_back();
//     return true;
// }

// template<class T>
//     typename HandleArray<T>::value_type* HandleArray<T>::at(const handle_value_type& handleValue)
// {
//     return const_cast<value_type*>(static_cast <const HandleArray<T>*> (this)->at(handleValue));
// }

// template<class T>
//     const typename HandleArray<T>::value_type* HandleArray<T>::at(const handle_value_type& handleValue) const
//     {
//         const auto index = getHandleIndex(handleValue);
// const auto counter = getHandleCounter(handleValue);
// assert(index >= 0 && index<m_handleMappers.size());

//         const auto& handleMapper = m_handleMappers[index];
//         if (handleMapper.counter != counter || !handleMapper.isActive)
//         {
//             return nullptr;
//         }

// assert(handleMapper.dataIndex >= 0 && handleMapper.dataIndex<m_data.size());
//         return &m_data[handleMapper.dataIndex];
//     }

//     template<class T>
//     typename HandleArray<T>::value_type* HandleArray<T>::operator[] (const handle_value_type& handleValue)
// {
//     return at(handleValue);
// }

// template<class T>
//     const typename HandleArray<T>::value_type* HandleArray<T>::operator[] (const handle_value_type& handleValue) const
//     {
//         return at(handleValue);
//     }


// template<class T>
//     inline HandleArrayBase::handle_value_type HandleArray<T>::getHandleValue(const handle_counter_type& counter, const handle_index_type& index)
// {
//     return ((handle_value_type)counter << (sizeof(handle_value_type) * 4)) + (handle_value_type)index;
// }

// template<class T>
//     inline HandleArrayBase::handle_index_type HandleArray<T>::getHandleIndex(const handle_value_type& value)
// {
//     return (handle_index_type)((value << (sizeof(handle_value_type) * 4)) >> (sizeof(handle_value_type) * 4));
// }

// template<class T>
//     inline HandleArrayBase::handle_counter_type HandleArray<T>::getHandleCounter(const handle_value_type& value)
// {
//     return (handle_counter_type)(value >> (sizeof(handle_value_type) * 4));
// }
// }
