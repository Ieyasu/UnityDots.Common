// using Unity.Entities;

// namespace UnityDots
// {
//     public static class ComponentDataFromEntityExtensions
//     {
//         public static bool TryGetValue<T>(this BufferFromEntity<T> bufferFromEntity, Entity entity, out DynamicBuffer<T> value)
//                 where T : struct, IBufferElementData
//         {
//             if (!bufferFromEntity.Exists(entity))
//             {
//                 value = default;
//                 return false;
//             }

//             value = bufferFromEntity[entity];
//             return true;
//         }

//         public static bool TryGetValue<T>(this ComponentDataFromEntity<T> componentDataFromEntity, Entity entity, out T value)
//                 where T : struct, IComponentData
//         {
//             if (!componentDataFromEntity.Exists(entity))
//             {
//                 value = default;
//                 return false;
//             }

//             value = componentDataFromEntity[entity];
//             return true;
//         }
//     }
// }
