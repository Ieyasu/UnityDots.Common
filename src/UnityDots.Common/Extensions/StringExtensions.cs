using System.Text.RegularExpressions;

namespace UnityDots
{
    public static class StringExtensions
    {
        private static readonly Regex _camelCaseRegex = new Regex("(?=\\p{Lu}\\p{Ll})|(?<=\\p{Ll})(?=\\p{Lu})|(?<=\\p{L})(?=\\p{N})");
        private static readonly Regex _sanitizeRegex = new Regex("[^A-Za-z0-9_]");
        private static readonly Regex _readableRegex = new Regex("[^A-Za-z0-9-]");

        public static string VariableNameToReadable(this string str)
        {
            var sanitized = _readableRegex.Replace(str, string.Empty);
            var capitalized = sanitized.CapitalizeFirst();
            var camelCased = capitalized.SplitCamelCase();
            return camelCased;
        }

        public static string SplitCamelCase(this string str)
        {
            return _camelCaseRegex.Replace(str, " ").Trim();
        }

        public static string SanitizeVariableName(this string str, string defaultStr = "")
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return defaultStr;
            }

            str = _sanitizeRegex.Replace(str, string.Empty);
            if (string.IsNullOrWhiteSpace(str))
            {
                str = defaultStr;
            }
            return str;
        }

        public static string CapitalizeFirst(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }

            var chars = str.ToCharArray();
            for (var i = 0; i < chars.Length; ++i)
            {
                var c = chars[i];
                if (char.IsLetter(c))
                {
                    if (char.IsUpper(c))
                    {
                        return str;
                    }

                    chars[i] = char.ToUpper(c);
                    return new string(chars);
                }
            }

            return str;
        }

        public static string IncrementSuffixNumber(this string str)
        {
            var prefix = Regex.Match(str, "^\\D+").Value;
            var numberStr = Regex.Replace(str, "^\\D+", string.Empty);

            if (int.TryParse(numberStr, out int number))
            {
                return prefix + (number + 1);
            }
            return prefix + 1;
        }

        public static string FormatDistance(float distanceInMeters)
        {
            if (distanceInMeters < 1000)
            {
                return $"{(int)distanceInMeters}m";
            }

            var distanceInKilometers = distanceInMeters / 1000;
            return $"{distanceInKilometers:0.#}km";
        }
    }
}
