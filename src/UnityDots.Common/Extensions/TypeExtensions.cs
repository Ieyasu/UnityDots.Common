using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace UnityDots
{
    public static class TypeExtensions
    {
        private static Type[] _allTypes;

        public static Type[] GetAllTypes()
        {
            if (_allTypes == null)
            {
                _allTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).ToArray();
            }
            return _allTypes;
        }

        public static bool IsCastableTo(this Type from, Type to)
        {
            if (to.IsAssignableFrom(from))
            {
                return true;
            }

            return from.GetMethods(BindingFlags.Public | BindingFlags.Static).Any(
                m => m.ReturnType == to && (m.Name == "op_Implicit" || m.Name == "op_Explicit")
            );
        }

        public static bool IsImplementationOf(this Type type, Type baseType)
        {
            return type.IsClass && !type.IsAbstract && type.IsSubclassOf(baseType);
        }

        public static bool IsImplementationOfGeneric(this Type type, Type genericType)
        {
            if (type.IsAbstract)
            {
                return false;
            }

            while (type != null && type != typeof(object))
            {
                var currentType = type.IsGenericType ? type.GetGenericTypeDefinition() : type;
                if (genericType == currentType)
                {
                    return true;
                }
                type = type.BaseType;
            }
            return false;
        }

        public static FieldInfo GetFieldRecursive(this Type type, string name, BindingFlags bindingFlags = BindingFlags.Default)
        {
            bindingFlags |= BindingFlags.DeclaredOnly;
            while (type != null && type != typeof(object))
            {
                var field = type.GetField(name, bindingFlags);
                if (field != null)
                {
                    return field;
                }
                type = type.BaseType;
            }
            return null;
        }

        public static IEnumerable<FieldInfo> GetFieldsRecursive(this Type type, BindingFlags bindingFlags = BindingFlags.Default)
        {
            bindingFlags |= BindingFlags.DeclaredOnly;
            while (type != null && type != typeof(object))
            {
                foreach (var field in type.GetFields(bindingFlags))
                {
                    yield return field;
                }
                type = type.BaseType;
            }
        }

        public static IEnumerable<PropertyInfo> GetPropertiesRecursive(this Type type, BindingFlags bindingFlags = BindingFlags.Default)
        {
            bindingFlags |= BindingFlags.DeclaredOnly;
            while (type != null && type != typeof(object))
            {
                foreach (var property in type.GetProperties(bindingFlags))
                {
                    yield return property;
                }
                type = type.BaseType;
            }
        }

        public static IEnumerable<MethodInfo> GetMethodsRecursive(this Type type, BindingFlags bindingFlags = BindingFlags.Default)
        {
            bindingFlags |= BindingFlags.DeclaredOnly;
            while (type != null && type != typeof(object))
            {
                foreach (var method in type.GetMethods(bindingFlags))
                {
                    yield return method;
                }
                type = type.BaseType;
            }
        }
    }
}
