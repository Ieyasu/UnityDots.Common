using System;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots
{
    public static class ComputeShaderExtensions
    {
        public static void DispatchSafe(this ComputeShader shader, int kernelHandle, int width, int height, int depth)
        {

            DispatchInternal(shader, kernelHandle, width, height, depth, true);
        }

        public static void DispatchUnsafe(this ComputeShader shader, int kernelHandle, int width, int height, int depth)
        {
            DispatchInternal(shader, kernelHandle, width, height, depth, false);
        }

        private static void DispatchInternal(ComputeShader shader, int kernelHandle, int width, int height, int depth, bool safe)
        {
            shader.GetKernelThreadGroupSizes(kernelHandle, out uint tx, out uint ty, out uint tz);

            if (safe && (width % tx != 0 || height % ty != 0 || depth % tz != 0))
            {
                var error = $"Sizes ({width}, {height}, {depth}) must be divisible by threadgroup sizes ({tx}, {ty}, {tz}).";
                ThrowException(shader, kernelHandle, error);
            }

            var x = (int)ceil(width / (float)tx);
            var y = (int)ceil(height / (float)ty);
            var z = (int)ceil(depth / (float)tz);

            var threadGroupCount = x * y * z;
            if (threadGroupCount > 65535)
            {
                var error = $"Thread group count ({x} x {y} x {z} = {threadGroupCount}) is above the maximum allowed limit of 65535.";
                ThrowException(shader, kernelHandle, error);
            }

            shader.Dispatch(kernelHandle, x, y, z);
        }

        private static void ThrowException(ComputeShader shader, int kernelHandle, string error)
        {
            var msg = $"Error dispatching shader {shader}, kernel {kernelHandle}: {error}";
            throw new ArgumentException(msg);
        }
    }
}
