using UnityEngine;
using UnityEngine.Rendering;
using static Unity.Mathematics.math;

namespace UnityDots
{
    public static class CommandBufferExtensions
    {
        private static uint _id = 10000;

        public static int GenerateID()
        {
            return (int)_id++;
        }

        public static void ResetID()
        {
            _id = 0;
        }

        public static void DispatchComputeSafe(this CommandBuffer buffer, ComputeShader shader, string kernel, int width, int height, int depth)
        {
            var kernelID = shader.FindKernel(kernel);
            DispatchComputeSafeInternal(buffer, shader, kernelID, width, height, depth);
        }

        public static void DispatchComputeSafe(this CommandBuffer buffer, ComputeShader shader, int kernelID, int width, int height, int depth)
        {
            DispatchComputeSafeInternal(buffer, shader, kernelID, width, height, depth);
        }

        public static void DispatchComputeUnsafe(this CommandBuffer buffer, ComputeShader shader, string kernel, int width, int height, int depth)
        {
            var kernelID = shader.FindKernel(kernel);
            DispatchComputeUnsafeInternal(buffer, shader, kernelID, width, height, depth);
        }

        public static void DispatchComputeUnsafe(this CommandBuffer buffer, ComputeShader shader, int kernelID, int width, int height, int depth)
        {
            DispatchComputeUnsafeInternal(buffer, shader, kernelID, width, height, depth);
        }

        public static void GetClearedTemporaryRT(this CommandBuffer buffer, int id, RenderTextureDescriptor descriptor)
        {
            GetClearedTemporaryRT(buffer, id, descriptor, Color.clear);
        }

        public static void GetClearedTemporaryRT(this CommandBuffer buffer, int id, RenderTextureDescriptor descriptor, Color color)
        {
            buffer.GetTemporaryRT(id, descriptor);
            buffer.SetRenderTarget(id);
            buffer.ClearRenderTarget(true, true, color, 1);
        }

        private static void DispatchComputeUnsafeInternal(CommandBuffer buffer, ComputeShader shader, int kernelID, int width, int height, int depth)
        {
            shader.GetKernelThreadGroupSizes(kernelID, out uint tx, out uint ty, out uint tz);
            var x = (int)ceil(width / (float)tx);
            var y = (int)ceil(height / (float)ty);
            var z = (int)ceil(depth / (float)tz);
            buffer.DispatchCompute(shader, kernelID, x, y, z);
        }

        private static void DispatchComputeSafeInternal(CommandBuffer buffer, ComputeShader shader, int kernelID, int width, int height, int depth)
        {
            shader.GetKernelThreadGroupSizes(kernelID, out uint x, out uint y, out uint z);
            if (width % x != 0 || height % y != 0 || depth % z != 0)
            {
                throw new System.ArgumentException("Sizes must be divisible by threadgroup sizes.");
            }
            buffer.DispatchCompute(shader, kernelID, width / (int)x, height / (int)y, depth / (int)z);
        }
    }
}
