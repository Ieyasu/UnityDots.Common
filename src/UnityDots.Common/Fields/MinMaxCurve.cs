using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using UnityDots.Mathematics;

namespace UnityDots
{
    [Serializable]
    public struct MinMaxCurve : IEquatable<MinMaxCurve>
    {
        [SerializeField]
        private MinMaxCurveMode _mode;

        [SerializeField]
        private float _constantMin;

        [SerializeField]
        private float _constantMax;

        [SerializeField]
        private AnimationCurve _curve;

        public MinMaxCurveMode Mode
        {
            get => _mode;
            set => _mode = value;
        }

        public float Constant
        {
            get => _constantMax;
            set => _constantMax = value;
        }

        public float ConstantMin
        {
            get => _constantMin;
            set => _constantMin = value;
        }

        public float ConstantMax
        {
            get => _constantMax;
            set => _constantMax = value;
        }

        public AnimationCurve Curve
        {
            get => _curve;
            set => _curve = value;
        }

        public MinMaxCurve(float constant)
            : this(0, constant)
        {
            _mode = MinMaxCurveMode.Constant;
        }

        public MinMaxCurve(float constantMin, float constantMax)
        {
            _mode = MinMaxCurveMode.TwoConstants;
            _curve = AnimationCurve.Linear(0, 0, 1, 1);
            _constantMin = constantMin;
            _constantMax = constantMax;
        }

        public Rect CalculateBounds()
        {
            switch (Mode)
            {
                case MinMaxCurveMode.Constant:
                    return new Rect(0, Constant, 0, 0);
                case MinMaxCurveMode.TwoConstants:
                    return new Rect(0, ConstantMin, 0, ConstantMax - ConstantMin);
                case MinMaxCurveMode.Curve:
                    return CalculateCurveBounds();
                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(Mode.ToString());
            }
        }

        private Rect CalculateCurveBounds()
        {
            if (Curve.length == 0)
            {
                return new Rect();
            }

            var firstKey = Curve.keys[0];
            var lastKey = Curve.keys[Curve.length - 1];
            var minVal = firstKey.value;
            var maxVal = lastKey.value;
            for (var i = 0; i < Curve.length - 1; ++i)
            {
                var key1 = Curve.keys[i];
                var key2 = Curve.keys[i + 1];
                var p0 = key1.value;
                var p3 = key2.value;
                var delta = (key2.time - key1.time) / 3.0f;
                var p1 = p0 + delta * key1.outTangent;
                var p2 = p3 - delta * key2.inTangent;
                var segmentBounds = Geometry.GetBoundsBezierCubic(p0, p1, p2, p3);
                minVal = min(minVal, segmentBounds.x);
                maxVal = max(maxVal, segmentBounds.y);
            }

            return new Rect()
            {
                xMin = firstKey.time,
                xMax = lastKey.time,
                yMin = lerp(ConstantMin, ConstantMax, minVal),
                yMax = lerp(ConstantMin, ConstantMax, maxVal)
            };
        }

        public override bool Equals(object obj)
        {
            return obj is MinMaxCurve other && Equals(other);
        }

        public bool Equals(MinMaxCurve other)
        {
            return Mode == other.Mode
                    && ConstantMin == other.ConstantMin
                    && ConstantMax == other.ConstantMax
                    && EqualityComparer<AnimationCurve>.Default.Equals(Curve, other.Curve);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Mode, ConstantMin, ConstantMax, Curve);
        }
    }
}
