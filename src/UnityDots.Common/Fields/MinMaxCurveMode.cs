namespace UnityDots
{
    public enum MinMaxCurveMode
    {
        Constant = 0,
        Curve = 1,
        TwoConstants = 2
    }
}
