using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class WindowDraggable : MouseManipulator
    {
        private bool _isActive;
        private Vector2 _mouseOffset;
        private VisualElement _handle;

        public WindowDraggable(VisualElement handle = null, VisualElement container = null)
        {
            _handle = handle;
            _isActive = false;

            if (container != null)
            {
                container.RegisterCallback<GeometryChangedEvent>(OnParentGeometryChanged);
            }
        }

        protected override void RegisterCallbacksOnTarget()
        {
            if (_handle == null)
            {
                _handle = target;
            }

            target.RegisterCallback<GeometryChangedEvent>(OnGeometryChanged);
            _handle.RegisterCallback(new EventCallback<MouseDownEvent>(OnMouseDown), TrickleDown.NoTrickleDown);
            _handle.RegisterCallback(new EventCallback<MouseMoveEvent>(OnMouseMove), TrickleDown.NoTrickleDown);
            _handle.RegisterCallback(new EventCallback<MouseUpEvent>(OnMouseUp), TrickleDown.NoTrickleDown);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<GeometryChangedEvent>(OnGeometryChanged);
            _handle.UnregisterCallback(new EventCallback<MouseDownEvent>(OnMouseDown), TrickleDown.NoTrickleDown);
            _handle.UnregisterCallback(new EventCallback<MouseMoveEvent>(OnMouseMove), TrickleDown.NoTrickleDown);
            _handle.UnregisterCallback(new EventCallback<MouseUpEvent>(OnMouseUp), TrickleDown.NoTrickleDown);
        }

        void OnMouseDown(MouseDownEvent e)
        {
            _isActive = true;

            // _localMouseOffset is offset from the target element's (0, 0) to the
            // to the mouse position.
            _mouseOffset = _handle.WorldToLocal(e.mousePosition);

            _handle.CaptureMouse();
            e.StopImmediatePropagation();
        }

        void OnMouseMove(MouseMoveEvent e)
        {
            if (_isActive)
            {
                // The mouse position of is corrected according to the offset within the target
                // element (m_LocalWorldOffset) to set the position relative to the mouse position
                // when the dragging started.
                var position = target.parent.WorldToLocal(e.mousePosition) - _mouseOffset;

                // Make sure that the object remains in the parent window
                position.x = Mathf.Clamp(position.x, 0f, target.parent.layout.width - target.layout.width);
                position.y = Mathf.Clamp(position.y, 0f, target.parent.layout.height - target.layout.height);

                // While moving, use only the left and top position properties,
                // while keeping the others NaN to not affect layout.
                target.style.left = position.x;
                target.style.top = position.y;
                target.style.right = float.NaN;
                target.style.bottom = float.NaN;
            }
        }

        void OnMouseUp(MouseUpEvent e)
        {
            _isActive = false;

            if (_handle.HasMouseCapture())
            {
                _handle.ReleaseMouse();
            }

            e.StopImmediatePropagation();
        }

        void OnGeometryChanged(GeometryChangedEvent geometryChangedEvent)
        {
            // Make the target clamp to the border of the window if the
            // parent window becomes too small to contain it.
            if (target.parent.layout.width < target.layout.width)
            {
                target.style.left = float.NaN;
                target.style.right = 0f;
            }

            if (target.parent.layout.height < target.layout.height)
            {
                target.style.top = float.NaN;
                target.style.bottom = 0f;
            }
        }

        void OnParentGeometryChanged(GeometryChangedEvent geometryChangedEvent)
        {
            // Check if the parent window can no longer contain the target window.
            // If the window is out of bounds, make one edge clamp to the border of the
            // parent window.
            if (target.layout.xMin < 0f)
            {
                target.style.left = 0f;
                target.style.right = float.NaN;
            }

            if (target.layout.xMax > geometryChangedEvent.newRect.width)
            {
                target.style.left = float.NaN;
                target.style.right = 0f;
            }

            if (target.layout.yMax > geometryChangedEvent.newRect.height)
            {
                target.style.top = float.NaN;
                target.style.bottom = 0f;
            }

            if (target.layout.yMin < 0f)
            {
                target.style.top = 0f;
                target.style.bottom = float.NaN;
            }
        }
    }
}
