using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class Draggable : MouseManipulator
    {
        private bool _isActive;
        private readonly Action<MouseMoveEvent> _handler;

        public Draggable(Action<MouseMoveEvent> handler)
        {
            _handler = handler;
            _isActive = false;
            activators.Add(new ManipulatorActivationFilter()
            {
                button = MouseButton.LeftMouse
            });
        }

        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback(new EventCallback<MouseDownEvent>(OnMouseDown), TrickleDown.NoTrickleDown);
            target.RegisterCallback(new EventCallback<MouseMoveEvent>(OnMouseMove), TrickleDown.NoTrickleDown);
            target.RegisterCallback(new EventCallback<MouseUpEvent>(OnMouseUp), TrickleDown.NoTrickleDown);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback(new EventCallback<MouseDownEvent>(OnMouseDown), TrickleDown.NoTrickleDown);
            target.UnregisterCallback(new EventCallback<MouseMoveEvent>(OnMouseMove), TrickleDown.NoTrickleDown);
            target.UnregisterCallback(new EventCallback<MouseUpEvent>(OnMouseUp), TrickleDown.NoTrickleDown);
        }

        void OnMouseDown(MouseDownEvent e)
        {
            target.CaptureMouse();
            _isActive = true;
            e.StopPropagation();
        }

        void OnMouseMove(MouseMoveEvent e)
        {
            if (_isActive)
            {
                _handler(e);
            }
        }

        void OnMouseUp(MouseUpEvent e)
        {
            _isActive = false;

            if (target.HasMouseCapture())
            {
                target.ReleaseMouse();
            }

            e.StopPropagation();
        }
    }
}
