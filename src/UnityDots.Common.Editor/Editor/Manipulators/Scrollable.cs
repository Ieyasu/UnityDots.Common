using System;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class Scrollable : MouseManipulator
    {
        private readonly Action<float> _handler;

        public Scrollable(Action<float> handler)
        {
            _handler = handler;
        }

        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<WheelEvent>(HandleMouseWheelEvent);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<WheelEvent>(HandleMouseWheelEvent);
        }

        void HandleMouseWheelEvent(WheelEvent e)
        {
            _handler(e.delta.y);
            e.StopPropagation();
        }
    }
}
