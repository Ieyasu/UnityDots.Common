using UnityEditor.UIElements;
using UnityEngine.UIElements;
using static Unity.Mathematics.math;

namespace UnityDots.Editor
{
    public sealed class SliderField : VisualElement
    {
        public FloatField Field { get; }
        public Slider Slider { get; }

        public string BindingPath
        {
            get => Slider.bindingPath;
            set
            {
                Slider.bindingPath = value;
                Field.bindingPath = value;
            }
        }

        public SliderField(string label, float start, float end, int precision = 2)
        {
            this.LoadAndAddElementStyleSheet("SliderField");

            Slider = new Slider(label, start, end);
            Field = new FloatField(2 + precision)
            {
                formatString = "0." + new string('#', precision),
                isDelayed = true
            };

            Field.RegisterValueChangedCallback(e =>
            {
                var value = clamp(e.newValue, Slider.lowValue, Slider.highValue);
                Field.SetValueWithoutNotify(value);
                e.StopPropagation();
            });

            Add(Slider);
            Add(Field);
            AddToClassList("unitydots-slider-field");
        }
    }
}
