using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class SliderIntField : VisualElement
    {
        public IntegerField Field { get; }
        public SliderInt Slider { get; }

        public string BindingPath
        {
            get => Slider.bindingPath;
            set
            {
                Slider.bindingPath = value;
                Field.bindingPath = value;
            }
        }

        public SliderIntField(string label, int start, int end)
        {
            this.LoadAndAddElementStyleSheet("SliderField");

            Slider = new SliderInt(label, start, end);
            Field = new IntegerField()
            {
                isDelayed = true
            };

            Field.RegisterValueChangedCallback(e =>
            {
                var value = Mathf.Clamp(e.newValue, Slider.lowValue, Slider.highValue);
                Field.SetValueWithoutNotify(value);
                e.StopPropagation();
            });

            Add(Slider);
            Add(Field);
            AddToClassList("unitydots-slider-field");
        }
    }
}
