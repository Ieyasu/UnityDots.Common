using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using static Unity.Mathematics.math;

namespace UnityDots.Editor
{
    public sealed class ClampedCurveField : CurveField
    {
        public float MinTime => ranges.xMin;
        public float MaxTime => ranges.xMax;
        public float MinValue => ranges.yMin;
        public float MaxValue => ranges.yMax;

        public string BindingPath
        {
            get => bindingPath;
            set => bindingPath = value;
        }

        public ClampedCurveField(Rect ranges)
            : this(null, ranges)
        {
        }

        public ClampedCurveField(string label, Rect ranges)
            : base(label)
        {
            this.ranges = ranges;

            this.RegisterValueChangedCallback(e =>
            {
                ValidateCurve(e.newValue);
            });
        }

        private void ValidateCurve(AnimationCurve curve)
        {
            if (curve.length == 0)
            {
                curve.AddKey(MinTime, MinValue);
                curve.AddKey(MaxTime, MaxValue);
            }

            if (curve.length == 1)
            {
                var key = curve.keys[0];
                key.time = MinTime;
                key.value = MinValue;
                curve.MoveKey(0, key);
                curve.AddKey(MaxTime, MaxValue);
            }

            // Make sure no keys overlap as overlapping keys will get removed
            for (var i = 1; i < curve.length - 1; ++i)
            {
                var minTime = MinTime + i * 0.001f;
                var maxTime = MaxTime - (curve.length - i - 1) * 0.001f;
                ClampKey(curve, i, minTime, maxTime);
            }

            ClampKey(curve, 0, MinTime, MinTime);
            ClampKey(curve, curve.length - 1, MaxTime, MaxTime);

            SetValueWithoutNotify(curve);
        }

        private void ClampKey(AnimationCurve curve, int index, float minTime, float maxTime)
        {
            var key = curve.keys[index];
            key.time = clamp(key.time, minTime, maxTime);
            key.value = clamp(key.value, MinValue, MaxValue);
            curve.MoveKey(index, key);
        }
    }
}
