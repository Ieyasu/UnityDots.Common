using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class Frame : VisualElement
    {
        public Toggle Header { get; }
        public VisualElement Body => Foldout.contentContainer;
        private Foldout Foldout { get; }

        public string BindingPath
        {
            get => Foldout.bindingPath;
            set => Foldout.bindingPath = value;
        }

        public Frame()
            : this(null)
        {
        }

        public Frame(string title)
        {
            this.LoadAndAddElementStyleSheet("Frame");

            Foldout = new Foldout();
            Header = Foldout.Q<Toggle>();
            Header.text = title;

            Add(Foldout);

            AddToClassList("unitydots-frame");
            Foldout.AddToClassList("unitydots-frame__foldout");
            Header.AddToClassList("unitydots-frame__header");
            Body.AddToClassList("unitydots-frame__content");
        }
    }
}
