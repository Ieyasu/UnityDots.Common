using System.ComponentModel;
using Unity.Mathematics;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class MinMaxCurveField : VisualElement
    {
        public EnumField Mode { get; private set; }
        public ClampedCurveField Curve { get; private set; }
        public FloatField Constant { get; private set; }
        public FloatField ConstantMin { get; private set; }
        public FloatField ConstantMax { get; private set; }

        public float MinValue { get; set; }
        public float MaxValue { get; set; }
        public int Precision { get; set; }

        public MinMaxCurveField(string label, SerializedProperty property)
            : this(label, property, -16777216, 16777216)
        {
        }

        public MinMaxCurveField(string label, SerializedProperty property, float minValue, float maxValue, int precision = 2)
        {
            this.LoadAndAddElementStyleSheet("MinMaxCurveField");

            MinValue = minValue;
            MaxValue = maxValue;
            Precision = precision;

            var modeProperty = property.FindPropertyRelative("_mode");

            Mode = new EnumField()
            {
                bindingPath = modeProperty.propertyPath
            };

            Constant = new FloatRangeField(label, MinValue, MaxValue)
            {
                bindingPath = property.FindPropertyRelative("_constantMax").propertyPath,
                formatString = "0." + new string('#', Precision),
            };

            ConstantMin = new FloatRangeField(label, MinValue, MaxValue)
            {
                bindingPath = property.FindPropertyRelative("_constantMin").propertyPath,
                formatString = "0." + new string('#', Precision),
            };

            ConstantMax = new FloatRangeField(MinValue, MaxValue)
            {
                bindingPath = property.FindPropertyRelative("_constantMax").propertyPath,
                formatString = "0." + new string('#', Precision),
            };

            Curve = new ClampedCurveField(new Rect(0, 0, 1, 1))
            {
                BindingPath = property.FindPropertyRelative("_curve").propertyPath,
            };

            var topRow = new VisualElement();
            topRow.Add(Constant);
            topRow.Add(ConstantMin);
            topRow.Add(ConstantMax);
            topRow.Add(Mode);

            Add(topRow);
            Add(Curve);

            UpdateControls(modeProperty.enumNames[modeProperty.enumValueIndex]);
            Mode.RegisterValueChangedCallback(e =>
            {
                if (e.newValue != null)
                {
                    UpdateControls(e.newValue.ToString());
                }
            });

            AddToClassList("unitydots-minmax-curve-field");
            topRow.AddToClassList("unitydots-minmax-curve-field__top-row");
        }

        private void UpdateControls(string value)
        {
            Constant.style.display = DisplayStyle.None;
            ConstantMin.style.display = DisplayStyle.None;
            ConstantMax.style.display = DisplayStyle.None;
            Curve.style.display = DisplayStyle.None;

            if (System.Enum.TryParse(value.Replace(" ", string.Empty), true, out MinMaxCurveMode mode))
            {
                switch (mode)
                {
                    case MinMaxCurveMode.Constant:
                        Constant.style.display = DisplayStyle.Flex;
                        break;
                    case MinMaxCurveMode.TwoConstants:
                        ConstantMin.style.display = DisplayStyle.Flex;
                        ConstantMax.style.display = DisplayStyle.Flex;
                        break;
                    case MinMaxCurveMode.Curve:
                        ConstantMin.style.display = DisplayStyle.Flex;
                        ConstantMax.style.display = DisplayStyle.Flex;
                        Curve.style.display = DisplayStyle.Flex;
                        break;
                    default:
                        throw new InvalidEnumArgumentException(mode.ToString());
                }
            }
        }
    }
}
