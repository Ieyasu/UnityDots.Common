using UnityEditor;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    [CustomPropertyDrawer(typeof(MinMaxCurve))]
    public sealed class MinMaxCurveDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            return new MinMaxCurveField(property.displayName, property, -float.MaxValue, float.MaxValue);
        }
    }
}
