using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using System;
using UnityEditor;

namespace UnityDots.Editor
{
    public sealed class ListField : VisualElement
    {
        private static readonly Regex _elementIndexRegex = new Regex(@"\[(\d+)\]$", RegexOptions.Compiled);
        private static readonly Icon _addIcon = Icon.Add;
        private static readonly Icon _removeIcon = Icon.Remove;
        private static readonly Icon _dragIcon = Icon.BuiltIn("align_vertically_center");

        private readonly Frame _frame;
        private readonly ListView _listView;
        private readonly SerializedProperty _listProperty;

        public Action AddItem { get; set; }
        public Action<int> RemoveItem { get; set; }

        public string Text
        {
            get => _frame.Header.text;
            set => _frame.Header.text = value;
        }

        public Func<VisualElement> MakeListItem
        {
            get => _listView.makeItem;
            set => _listView.makeItem = value;
        }

        public Action<VisualElement, int> BindListItem
        {
            get => _listView.bindItem;
            set => _listView.bindItem = value;
        }

        public ListField(SerializedProperty property, int itemHeight)
        {
            this.LoadAndAddElementStyleSheet("ListField");

            AddItem = DefaultAddItem;
            RemoveItem = DefaultRemoveItem;

            _listProperty = property;

            _frame = new Frame()
            {
                BindingPath = property.propertyPath
            };

            _listView = new ListView()
            {
                bindingPath = property.propertyPath,
                makeItem = DefaultMakeListItem,
                itemHeight = itemHeight + 8,
                reorderable = true,
                showBoundCollectionSize = false,
                showAlternatingRowBackgrounds = AlternatingRowBackground.ContentOnly,
            };

            Add(_frame);
            _frame.Header.Add(new IconButton(() => AddItem()) { image = _addIcon.Texture });
            _frame.Body.Add(_listView);

            AddToClassList("unitydots-list-field");
            _listView.AddToClassList("unitydots-list-field__list-view");
        }

        public void Refresh()
        {
            _listView.Refresh();
        }

        private void DefaultAddItem()
        {
            _listProperty.serializedObject.Update();
            _listProperty.InsertArrayElementAtIndex(0);
            _listProperty.serializedObject.ApplyModifiedProperties();
        }

        private void DefaultRemoveItem(int index)
        {
            _listProperty.serializedObject.Update();
            _listProperty.DeleteArrayElementAtIndex(index);
            _listProperty.serializedObject.ApplyModifiedProperties();
        }

        private int GetIndex(PropertyField field)
        {
            var match = _elementIndexRegex.Match(field.bindingPath);
            var value = match.Groups[1].Value;

            if (!int.TryParse(value, out int index))
            {
                Debug.LogError($"Failed to remove item: failed to parse index from binding path {field.bindingPath}");
                return -1;
            }

            return index;
        }

        private VisualElement DefaultMakeListItem()
        {
            var item = new VisualElement();
            item.AddToClassList("unitydots-list-field__item");

            var dragHandle = new Image() { image = _dragIcon.Texture };
            dragHandle.AddToClassList("unitydots-list-field__item-drag-handle");
            item.Add(dragHandle);

            var itemValue = new PropertyField();
            item.Add(itemValue);
            itemValue.AddToClassList("unitydots-list-field__item-value");

            var removeButton = new IconButton() { image = _removeIcon.Texture };
            removeButton.Clicked += () => RemoveItem(GetIndex(itemValue));
            removeButton.AddToClassList("unitydots-list-field__item-remove-btn");
            item.Add(removeButton);

            return item;
        }
    }
}
