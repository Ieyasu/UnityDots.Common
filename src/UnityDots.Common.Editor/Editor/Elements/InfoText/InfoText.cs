using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class InfoText : VisualElement
    {
        public Label Label { get; }
        public Label Info { get; }

        public InfoText(string labelText, string infoText)
        {
            this.LoadAndAddElementStyleSheet("InfoText");

            Label = new Label(labelText);
            Info = new Label(infoText);

            Add(Label);
            Add(Info);

            AddToClassList("unity-base-field");
            AddToClassList("unitydots-info-text-field");
            Label.AddToClassList("unity-base-field__label");
        }
    }
}
