using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public abstract class PickerField<T> : BaseField<T>
    {
        public const string EmptyLabel = "<None>";
        public const string Stylesheet = "PickerField";
        public const string UssClassName = "unitydots-picker-field";
        public const string InputUssClassName = UssClassName + "__input";
        public const string ButtonUssClassName = InputUssClassName + "__button";
        public const string IconUssClassName = ButtonUssClassName + "__icon";
        public const string InputLabelUssClassName = ButtonUssClassName + "__label";
        public const string ArrowUssClassName = ButtonUssClassName + "__arrow";

        private VisualElement _visualInput;
        private Image _icon;
        private TextElement _label;

        protected PickerProvider<T> Provider { get; set; }

        protected PickerField(string label)
            : this(label, CreateVisualInput())
        {
        }

        private PickerField(string label, VisualElement visualInput)
            : base(label, visualInput)
        {
            _visualInput = visualInput;
            _icon = _visualInput.Q<Image>();
            _label = _visualInput.Q<TextElement>();

            _visualInput.Q<Button>().clicked += () =>
            {
                if (Provider)
                {
                    var worldPoint = new Vector2(worldBound.center.x, worldBound.yMax + worldBound.height - 4);
                    var screenPoint = GUIUtility.GUIToScreenPoint(worldPoint);
                    SearchWindow.Open(new SearchWindowContext(screenPoint, worldBound.width), Provider);
                }
            };

            _visualInput.AddToClassList(InputUssClassName);
            AddToClassList(UssClassName);
            this.LoadAndAddElementStyleSheet(Stylesheet);
        }

        protected void SetLabel(Texture icon, string text)
        {
            ((INotifyValueChanged<string>)_label).SetValueWithoutNotify(text);
            _icon.image = icon;
            _icon.SetVisible(icon);
        }

        private static VisualElement CreateVisualInput()
        {
            var visualInput = new VisualElement();
            var button = new Button();
            button.AddToClassList(ButtonUssClassName);

            var icon = new Image { pickingMode = PickingMode.Ignore };
            icon.AddToClassList(IconUssClassName);

            var label = new TextElement { pickingMode = PickingMode.Ignore };
            label.AddToClassList(InputLabelUssClassName);

            var arrow = new VisualElement { pickingMode = PickingMode.Ignore };
            arrow.AddToClassList(ArrowUssClassName);
            arrow.AddToClassList(BasePopupField<T, T>.arrowUssClassName);

            button.Add(icon);
            button.Add(label);
            button.Add(arrow);

            visualInput.Add(button);
            return visualInput;
        }
    }
}
