using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class IconButton : Image
    {
        public const string Stylesheet = "IconButton";
        public const string UssClassName = "unitydots-icon-button";

        private static readonly Dictionary<string, Icon> _icons = new Dictionary<string, Icon>
        {
            { "Add", Icon.Add },
            { "CustomAdd", Icon.CustomAdd },
            { "Remove", Icon.Remove },
            { "Inspect", Icon.Inspect },
            { "Expanded", Icon.Expanded },
            { "Collapsed", Icon.Collapsed },
            { "Refresh", Icon.Refresh },
            { "Load", Icon.Load },
            { "Unload", Icon.Unload },
            { "Close", Icon.Close },
            { "LeftArrow", Icon.LeftArrow },
            { "RightArrow", Icon.RightArrow },
            { "Info", Icon.Info },
            { "Warning", Icon.Warning },
            { "Error", Icon.Error },
            { "Settings", Icon.Settings },
            { "View", Icon.View },
            { "Locked", Icon.Locked },
            { "Unlocked", Icon.Unlocked }
        };

        private Clickable _clickable;
        public event Action Clicked
        {
            add
            {
                if (_clickable == null)
                {
                    _clickable = new Clickable(value);
                    this.AddManipulator(_clickable);
                }
                else
                {
                    _clickable.clicked += value;
                }
            }
            remove
            {
                if (_clickable != null)
                {
                    _clickable.clicked -= value;
                }
            }
        }

        public IconButton() : this(null)
        {
        }

        public IconButton(Action clickEvent)
        {
            Clicked += clickEvent;

            AddToClassList(UssClassName);
            this.LoadAndAddElementStyleSheet(Stylesheet);
        }

        public void SetIcon(string iconName)
        {
            if (_icons.TryGetValue(iconName, out var icon))
            {
                image = icon.Texture;
            }
            else
            {
                Debug.LogWarning($"Unknown icon '{iconName}' for IconButton: the icon could not be found");
            }
        }
    }
}
