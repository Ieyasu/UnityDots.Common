using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public sealed class Title : Label
    {
        public Title(string text)
            : base(text)
        {
            this.LoadAndAddElementStyleSheet("Title");
            AddToClassList("unitydots-title");
        }
    }
}
