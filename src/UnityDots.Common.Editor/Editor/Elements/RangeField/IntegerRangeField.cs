using UnityEditor.UIElements;
using UnityEngine.UIElements;
using static Unity.Mathematics.math;

namespace UnityDots.Editor
{
    public sealed class IntegerRangeField : IntegerField
    {
        public IntegerRangeField(string label, int min, int max)
            : base(label)
        {
            this.RegisterValueChangedCallback(e =>
            {
                var value = clamp(e.newValue, min, max);
                if (value != e.newValue)
                {
                    SetValueWithoutNotify(value);
                }
            });
        }
    }
}
