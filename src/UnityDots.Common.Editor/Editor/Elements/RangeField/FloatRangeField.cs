using Unity.Mathematics;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using static Unity.Mathematics.math;

namespace UnityDots.Editor
{
    public sealed class FloatRangeField : FloatField
    {
        public FloatRangeField(float min, float max)
            : this(null, min, max)
        {
        }

        public FloatRangeField(string label, float min, float max)
            : base(label)
        {
            this.RegisterValueChangedCallback(e =>
            {
                var value = clamp(e.newValue, min, max);
                if (value != e.newValue)
                {
                    SetValueWithoutNotify(value);
                }
            });
        }
    }
}
