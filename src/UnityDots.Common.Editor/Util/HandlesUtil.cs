using UnityEditor;
using UnityEngine;

namespace UnityDots.Editor
{
    public static class HandlesUtil
    {
        private static readonly Vector3[] _aaLine = new Vector3[2];

        public static void DrawAALine(float width, Vector3 p0, Vector3 p1)
        {
            _aaLine[0] = p0;
            _aaLine[1] = p1;
            Handles.DrawAAPolyLine(width, 2, _aaLine);
        }
    }
}
