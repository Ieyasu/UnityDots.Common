using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UnityDots.Editor
{
    public sealed class AssetList
    {

        public Type Type { get; }
        public List<Object> Assets { get; set; }
        public List<string> Paths { get; set; }

        public AssetList(Type type)
        {
            Type = type;
            Assets = new List<Object>();
            Paths = new List<string>();
        }
    }

    public static class AssetDatabaseHelper
    {
        private static readonly Dictionary<string, AssetList> _assetLists = new Dictionary<string, AssetList>();
        private static readonly Regex _assetPathExpression = new Regex(@".*/(Assets/.*/)[^/]+", RegexOptions.Compiled);
        private static readonly Regex _packagePathExpression = new Regex(@".*/PackageCache(/[^/]+)@[^/]+(/.*/)[^/]+", RegexOptions.Compiled);
        private static readonly string _packageFormat = "Packages{0}{1}";

        public static void DiscardChanges(Object asset)
        {
            Resources.UnloadAsset(asset);

            var path = AssetDatabase.GetAssetPath(asset);
            AssetDatabase.ImportAsset(path);
        }

        public static void AddSubassetIfMissing(Object asset, Object subasset)
        {
            if (!AssetDatabase.IsSubAsset(subasset))
            {
                AssetDatabase.AddObjectToAsset(subasset, asset);

                var path = AssetDatabase.GetAssetPath(subasset);
                AssetDatabase.ImportAsset(path);
            }
        }

        public static void DestroySubassets(Object asset, string name)
        {
            var path = AssetDatabase.GetAssetPath(asset);
            var assets = AssetDatabase.LoadAllAssetsAtPath(path);
            foreach (var subasset in assets)
            {
                if (subasset != asset && subasset.name.Contains(name))
                {
                    Object.DestroyImmediate(subasset, true);
                }
            }

            AssetDatabase.ImportAsset(path);
        }

        public static void DestroySubassets<T>(Object asset) where T : Object
        {
            var path = AssetDatabase.GetAssetPath(asset);
            var assets = AssetDatabase.LoadAllAssetsAtPath(path);
            foreach (var subasset in assets)
            {
                if (subasset != asset && subasset is T)
                {
                    Object.DestroyImmediate(subasset, true);
                }
            }

            AssetDatabase.ImportAsset(path);
        }

        public static string GetAssetPath(string fullPath)
        {
            var normalizedPath = fullPath.Replace('\\', '/');

            var assetMatch = _assetPathExpression.Match(normalizedPath);
            if (assetMatch.Success)
            {
                return assetMatch.Groups[1].Value;
            }

            var packageMatch = _packagePathExpression.Match(normalizedPath);
            if (packageMatch.Success)
            {
                return string.Format(_packageFormat, packageMatch.Groups[1].Value, packageMatch.Groups[2].Value);
            }

            return Path.GetDirectoryName(normalizedPath);
        }

        public static AssetList GetAssetList<AssetType>() where AssetType : Object
        {
            return GetAssetList(typeof(AssetType));
        }

        public static Object GetAssetWithId(string id, Type type)
        {
            var path = AssetDatabase.GUIDToAssetPath(id);
            return AssetDatabase.LoadAssetAtPath(path, type);
        }

        public static IEnumerable<Object> GetAssets(Type assetType)
        {
            return AssetDatabase.FindAssets($"t:{assetType.Name}").Select(id => GetAssetWithId(id, assetType)).Where(asset => asset);
        }

        public static AssetList GetAssetList(Type assetType)
        {
            var listName = assetType.AssemblyQualifiedName;

            if (!_assetLists.TryGetValue(listName, out var list))
            {
                list = new AssetList(assetType);

                var assets = GetAssets(assetType);
                var paths = assets.Select(asset => GetPath(asset));
                var prefix = FindCommonPath(paths);

                list.Assets = assets.ToList();
                list.Paths = assets.Select(asset =>
                {
                    var path = GetPath(asset).Substring(prefix.Length);
                    return path.Length > 0 ? path + asset.name : asset.name;
                }).ToList();

                _assetLists.Add(listName, list);
            }

            return list;
        }

        private static string GetPath(Object asset)
        {
            var path = AssetDatabase.GetAssetPath(asset);
            var slash = path.LastIndexOf('/');

            return path.Substring(0, slash + 1);
        }

        private static string FindCommonPath(IEnumerable<string> paths)
        {
            var prefix = paths.FirstOrDefault() ?? string.Empty;

            foreach (var path in paths)
            {
                var index = 0;
                var count = Math.Min(prefix.Length, path.Length);

                for (; index < count; index++)
                {
                    if (prefix[index] != path[index])
                        break;
                }

                prefix = prefix.Substring(0, index);

                var slash = prefix.LastIndexOf('/');
                if (slash != prefix.Length - 1)
                    prefix = slash >= 0 ? prefix.Substring(0, slash + 1) : string.Empty;
            }

            return prefix;
        }

        public static Texture2DArray CreateTextureArray(IList<Texture2D> textures, bool linear)
        {
            var texture = textures.Where(x => x != null).FirstOrDefault();
            if (texture == null)
            {
                Debug.LogError("No textures defined. Can't create texture array.");
                return null;
            }

            var array = new Texture2DArray(texture.width, texture.height, textures.Count, texture.format, true, linear)
            {
                anisoLevel = texture.anisoLevel,
                wrapMode = texture.wrapMode,
                filterMode = texture.filterMode
            };

            for (int i = 0; i < textures.Count; ++i)
            {
                var layer = textures[i];
                if (layer != null)
                {
                    Graphics.CopyTexture(layer, 0, array, i);
                }
            }
            return array;
        }

        public static Texture2DArray CreateTextureArray(Object asset, string name, IList<Texture2D> textures, bool linear)
        {
            var array = CreateTextureArray(textures, linear);
            array.name = name;
            AssetDatabase.AddObjectToAsset(array, asset);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(asset));
            return array;
        }
    }
}
