using UnityEditor;
using UnityEditor.Rendering;
using UnityEngine;

namespace UnityDots.Editor
{
    public static class ComputeShaderExtensions
    {
        public static bool HasErrors(this ComputeShader shader)
        {
            var errors = ShaderUtil.GetComputeShaderMessages(shader);
            foreach (var error in errors)
            {
                if (error.severity == ShaderCompilerMessageSeverity.Error)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool HasWarning(this ComputeShader shader)
        {
            var messages = ShaderUtil.GetComputeShaderMessages(shader);
            foreach (var message in messages)
            {
                if (message.severity == ShaderCompilerMessageSeverity.Warning)
                {
                    return true;
                }
            }

            return false;
        }
    }
}


