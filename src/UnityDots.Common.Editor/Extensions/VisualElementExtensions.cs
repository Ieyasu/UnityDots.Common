using System;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace UnityDots.Editor
{
    public static class VisualElementExtensions
    {
        public static void SetVisible(this VisualElement element, bool visible)
        {
            element.style.display = visible ? DisplayStyle.Flex : DisplayStyle.None;
        }

        #region Style

        public static void LoadAndAddStyleSheet(this VisualElement element, string sheetPath)
        {
            var styleSheet = Resources.Load<StyleSheet>(sheetPath);

            if (styleSheet == null)
            {
                Debug.LogWarning($"Style sheet not found for path \"{sheetPath}\"");
            }
            else
            {
                element.styleSheets.Add(styleSheet);
            }
        }

        public static void LoadAndAddElementStyleSheet(this VisualElement element, string sheetName)
        {
            var sheetPath = $"Packages/com.unitydots.common/UnityDots.Common.Editor/Editor/Elements/{sheetName}/{sheetName}.uss";
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(sheetPath);

            if (styleSheet == null)
            {
                Debug.LogWarning($"Style sheet not found for path \"{sheetPath}\"");
            }
            else
            {
                element.styleSheets.Add(styleSheet);
            }
        }

        #endregion //Style

        public static PropertyField AddField(this VisualElement element, SerializedProperty property, string propertyName)
        {
            var field = new PropertyField(property.FindPropertyRelative(propertyName));
            element.Add(field);
            return field;
        }

        public static PropertyField AddField(this VisualElement element, SerializedProperty property, string propertyName, string label)
        {
            var field = new PropertyField(property.FindPropertyRelative(propertyName), label);
            element.Add(field);
            return field;
        }

        public static PropertyField AddField(this VisualElement element, SerializedObject serializedObject, string propertyName)
        {
            var field = new PropertyField(serializedObject.FindProperty(propertyName));
            element.Add(field);
            return field;
        }

        public static PropertyField AddField(this VisualElement element, SerializedObject serializedObject, string propertyName, string label)
        {
            var field = new PropertyField(serializedObject.FindProperty(propertyName), label);
            element.Add(field);
            return field;
        }

        #region Events

        public static void RegisterToAllChanges(this VisualElement element, Action action)
        {
            element.RegisterCallback<ChangeEvent<bool>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<int>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<uint>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<float>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<string>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<Enum>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<UnityEngine.Object>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<Vector2>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<Vector2Int>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<Vector3>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<Vector3Int>>(e => action.Invoke());
            element.RegisterCallback<ChangeEvent<AnimationCurve>>(e => action.Invoke());
        }

        public static void SendChangeEvent<T>(this VisualElement element, T previous, T current)
        {
            using (var changeEvent = ChangeEvent<T>.GetPooled(previous, current))
            {
                changeEvent.target = element;
                element.SendEvent(changeEvent);
            }
        }

        #endregion //Events
    }
}
